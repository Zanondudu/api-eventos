import Sequelize, { Model } from 'sequelize';

class Usuario extends Model {
  static init(sequelize) {
    super.init({
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      nome: Sequelize.STRING,
      senha: Sequelize.STRING,
      administrador: Sequelize.INTEGER,
    },
      {
        sequelize,
        freezeTableName: 'usuario',
        tableName: 'usuario'
      },
    );
    return this;
  }
}

export default Usuario;