import Sequelize, { Model } from 'sequelize';

class Local extends Model {
  static init(sequelize) {
    super.init({
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      nome: Sequelize.STRING,
    },
      {
        sequelize,
        freezeTableName: 'local',
        tableName: 'local'
      },
    );
    return this;
  }
}

export default Local;