import Sequelize, { Model } from 'sequelize';

class MotoristaHasEvento extends Model {
  static init(sequelize) {
    super.init({
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      id_evento: Sequelize.INTEGER,
      id_motorista: Sequelize.INTEGER,
    },
      {
        sequelize,
        freezeTableName: 'motorista_has_evento',
        tableName: 'motorista_has_evento'
      },
    );
    return this;
  }
  
  // static associate(models) {
  //   this.belongsTo(models.evento, {foreignKey: 'id', as: 'id_evento'});
  //   this.belongsTo(models.usuario, {foreignKey: 'id', as: 'id_motorista'});
  // }
}

export default MotoristaHasEvento;