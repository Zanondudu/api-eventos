import Sequelize, { Model } from 'sequelize';

class EventoHasUsuario extends Model {
  static init(sequelize) {
    super.init({
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      id_evento: Sequelize.INTEGER,
      id_participante: Sequelize.INTEGER,
    },
      {
        sequelize,
        freezeTableName: 'evento_has_usuario',
        tableName: 'evento_has_usuario'
      },
    );
    return this;
  }
  
  static associate(models) {
    // this.belongsTo(models.evento, {foreignKey: 'id', as: 'id_evento'});
    // this.belongsTo(models.usuario, {foreignKey: 'id', as: 'id_participante'});
  }
}

export default EventoHasUsuario;