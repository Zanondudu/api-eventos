import Sequelize, { Model } from 'sequelize';

class Evento extends Model {
  static init(sequelize) {
    super.init({
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      nome: Sequelize.STRING,
      descricao: Sequelize.STRING,
      id_local: Sequelize.INTEGER,
      id_categoria: Sequelize.INTEGER,
      id_organizador: Sequelize.INTEGER,
      valor: Sequelize.INTEGER,
    },
      {
        sequelize,
        freezeTableName: 'evento',
        tableName: 'evento'
      },
    );
    return this;
  }
  
  // static associate(models) {
  //   this.belongsTo(models.local, {foreignKey: 'id', as: 'id_local'});
  //   this.belongsTo(models.categoria, {foreignKey: 'id', as: 'id_categoria'});
  //   this.belongsTo(models.usuario, {foreignKey: 'id', as: 'id_organizador'});
  // }
}

export default Evento;