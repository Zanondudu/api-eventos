import Sequelize, { Model } from 'sequelize';

class Categoria extends Model {
  static init(sequelize) {
    super.init({
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      nome: Sequelize.STRING,
    },
      {
        sequelize,
        freezeTableName: 'categoria',
        tableName: 'categoria'
      },
    );
    return this;
  }
}

export default Categoria;