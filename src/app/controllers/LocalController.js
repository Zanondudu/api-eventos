import * as Yup from 'yup';
import { Op } from 'sequelize';
import local from '../models/local';

class LocalController {

  async getAllPlaces(req, res) {   
    
    const places = await local.findAll();
        
    if (!places) {
      return res.status(400).json({ error: 'Nenhum local encontrado' });
    }

    return res.json(places);
  }
  
  async getPlace(req, res) {
    const schema = Yup.object().shape({
      id: Yup.number().required()
    });

    if(!req.body.id){ 
      return res.status(400).json({error: 'informe os dados corretamente'});
    }
    
    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Informe os dados corretamente' });
    }
    
    const place = await local.findOne({
      where: {
        id: req.body.id
      },
      attributes: ['id', 'nome']
    });

    if (!place) {
      return res.status(400).json({ error: 'Local não existente' });
    }

    return res.json(place);
  }
  
}

export default new LocalController();