import jwt, { TokenExpiredError } from 'jsonwebtoken';
import * as Yup from 'yup';
import usuario from '../models/usuario';
import authConfig from '../../config/auth';

class SessionController {
  async session(req, res) {
        
    const schema = Yup.object().shape({
      nome: Yup.string().required(),
      senha: Yup.string().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Informe os dados corretamente' });
    }
            
    const name = req.body.nome;
    const password = req.body.senha;

    const existentUser = await usuario.findOne({
      where: {
        nome: name
      },
      attributes: ['id', 'nome', 'senha']
    });

    if (!existentUser) {
      return res.status(401).json({ error: 'Nome de usuário não encontrado' });
    }  
        
    if (password !== existentUser.senha) {
      return res.status(401).json({ error: 'Senha inválida' });
    }

    const userId = existentUser.id; 

    return res.json({
      user: {
        id: userId,
        name,
      },
      token: jwt.sign({ userId }, authConfig.secret, {
        expiresIn: authConfig.expiresIn
      })
    });
  }
}

export default new SessionController();