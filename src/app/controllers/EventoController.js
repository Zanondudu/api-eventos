import * as Yup from 'yup';
import evento from '../models/evento';
import eventoHasUsuario from '../models/eventoHasUsuario'

class UsuarioController {
  async store(req, res) {
    const schema = Yup.object().shape({
      nome: Yup.string().max(100).required(),
      descricao: Yup.string().max(255).required(),
      id_local: Yup.number().required(),
      id_categoria: Yup.number().required(),
      id_organizador: Yup.number().required(),
      valor: Yup.number()
    });

    if (!req.body.nome ||
      !req.body.descricao ||
      !req.body.id_local ||
      !req.body.id_categoria ||
      !req.body.id_organizador
    ) {
      return res.status(400).json({ error: 'informe os dados corretamente' });
    }

    if (!(schema.isValid(req.body))) {
      return res.status(400).json({ error: 'informe os dados corretamente' });
    }

    const createdEvent = await evento.create(req.body);

    return res.json({
      id: createdEvent.id,
      name: createdEvent.nome,
      description: createdEvent.descricao,
      id_place: createdEvent.id_local,
      id_category: createdEvent.id_categoria,
      id_organizer: createdEvent.id_organizador,
      price: createdEvent.valor,
    })
  }

  async getEvent(req, res) {
    const schema = Yup.object().shape({
      id: Yup.number().required(),
    });

    if (!req.body.id) {
      return res.status(400).json({ error: 'informe os dados corretamente' });
    }

    if (!(schema.isValid(req.body))) {
      return res.status(400).json({ error: 'informe os dados corretamente' });
    }

    const event = await evento.findOne({
      where: {
        id: req.body.id
      },
      attributes: [
        'id',
        'nome',
        'descricao',
        'id_local',
        'id_categoria',
        'id_organizador',
        'valor'
      ]
    });
    
    if (!event) {
      return res.status(400).json({ error: 'evento não encontrado' });
    }

    return res.json({
      id: event.id,
      name: event.nome,
      description: event.descricao,
      id_place: event.id_local,
      id_category: event.id_categoria,
      id_organizer: event.id_organizador,
      price: event.valor,
    })
  }
  
  async getAllEvents(req, res) {
    const events = await evento.findAll();

    if (!events) {
      return res.status(400).json({ error: 'Não há eventos' });
    }

    return res.json(events)
  }
  
  async confirmPresence(req, res) {
    const schema = Yup.object().shape({
      id_evento: Yup.number().required(),
      id_participante: Yup.number().required(),
    });

    if (!req.body.id_evento ||
      !req.body.id_participante
    ) {
      return res.status(400).json({ error: 'informe os dados corretamente' });
    }
        
    if (!(schema.isValid(req.body))) {
      return res.status(400).json({ error: 'informe os dados corretamente' });
    }
    
    const existentEvent = await evento.findOne({
      where: {
        id: req.body.id_evento
      },
      attributes: ['id']
    });

    if (!existentEvent) {
      return res.status(400).json({ error: 'Evento não encontrado' });
    }
    
    const alreadyConfirmed = eventoHasUsuario.findOne({
      where: {
        id_evento: req.body.id_evento,
        id_participante: req.body.id_participante
      },
      attributes: ['id', 'id_evento', 'id_participante']
    });
    
    if(alreadyConfirmed){
      return res.status(400).json({ error: 'Usuário já confirmado' });
    }

    const presence = await eventoHasUsuario.create(req.body);

    return res.json({
      id: presence.id,
      id_event: presence.id_evento,
      id_participant: presence.id_participante,
    })
  }
  
  async getPresences(req, res) {
    const schema = Yup.object().shape({
      id_evento: Yup.number().required(),
    });

    if (!req.body.id_evento) {
      return res.status(400).json({ error: 'informe os dados corretamente' });
    }
        
    if (!(schema.isValid(req.body))) {
      return res.status(400).json({ error: 'informe os dados corretamente' });
    }
        
    const presences = await eventoHasUsuario.findAll({
      where: {
        id_evento: req.body.id_evento
      },
      attributes: ['id', 'id_evento', 'id_participante']
    });

    return res.json(presences);
  }
}

export default new UsuarioController();