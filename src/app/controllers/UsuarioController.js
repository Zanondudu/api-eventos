import * as Yup from 'yup';
import { Op } from 'sequelize';
import usuario from '../models/usuario';

class UsuarioController {
  async store(req, res) {
    const schema = Yup.object().shape({
      nome: Yup.string().max(100).required(),
      senha: Yup.string().max(100).required()
    });
    
    if(!req.body.nome || !req.body.senha){ 
     return res.status(400).json({error: 'informe os dados corretamente'});
   }
    
    if(!(schema.isValid(req.body))){ 
      return res.status(400).json({error: 'informe os dados corretamente'});
    }
  
    const existentUser = await usuario.findOne({
      where: {
        nome: req.body.nome
      },
      attributes: ['nome',]
    });
    
    if(existentUser) {
      return res.status(400).json({error: 'Já existe um usuário com esse nome'});
    }
    
    const createdUser = await usuario.create(req.body);
    
    const id = createdUser.id;
    const name = createdUser.nome;
    
    return res.json({
      id,
      name, 
    })
    
  }
}

export default new UsuarioController();