import { Router } from 'express';
import SessionController from './app/controllers/SessionController';
import authMiddleware from '../src/app/middlewares/auth';
import UsuarioController from '../src/app/controllers/UsuarioController';
import CategoriaController from '../src/app/controllers/CategoriaController';
import LocalController from '../src/app/controllers/LocalController';
import EventoController from '../src/app/controllers/EventoController';

const routes = new Router();

routes.get('/test', (req, res) => {
  return res.json({ message: 'OK' });
});

routes.post('/session', SessionController.session);
routes.post('/create-user', UsuarioController.store);

routes.use(authMiddleware);

routes.get('/get-all-categories', CategoriaController.getAllCategories);
routes.get('/get-category', CategoriaController.getCategory);

routes.get('/get-all-places', LocalController.getAllPlaces);
routes.get('/get-place', LocalController.getPlace);

routes.post('/create-event', EventoController.store);
routes.get('/get-event', EventoController.getEvent);
routes.get('/get-all-events', EventoController.getAllEvents);
routes.post('/confirm-presence', EventoController.confirmPresence);
routes.get('/get-presences', EventoController.getPresences);


export default routes;