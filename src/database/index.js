
import Sequelize from 'sequelize';
import categoria from '../app/models/categoria';
import evento from '../app/models/evento';
import eventoHasUsuario from '../app/models/eventoHasUsuario';
import local from '../app/models/local';
import motoristaHasEvento from '../app/models/motoristaHasEvento';
import passageiroHasMotorista from '../app/models/passageiroHasMotorista';
import usuario from '../app/models/usuario';
import dbConfig from '../config/database';

const models = [
  categoria,
  evento,
  eventoHasUsuario,
  local,
  motoristaHasEvento,
  passageiroHasMotorista,
  usuario
];

class Database {
  constructor() {
    this.init();
  }
  init() {
    this.connection = new Sequelize(dbConfig);
    models
      .map(model => model.init(this.connection))
      .map(model => model.associate && model.associate(this.connection.models));
  }
}


export default new Database();
